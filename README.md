# Installation with single script

- Tested on Ubuntu 20.04 and 22.04

```bash -c "$(wget -O - https://gitlab.com/ethergem/egem-node-install/raw/master/ubuntu_install.sh)"```
- You can ignore the rest of this file, it is for the 3 script version.



# Under Construction
- This is the setup for a user that doesn't want to run our docker install.

# Working OS Versions
- Ubuntu 20

# What Happens?
- This script will download required apps and updates to then install golang and build egem from source.

# Setup
## Follow the steps one by one.

- Use sudo -i to enter root account
- Clone the repo
- Make the 3 files executable
```
chmod +x prep.sh install.sh start.sh
```
- Run egem prep.sh
- Enter the commands to add to golang profile.
- Then run the egem install.sh
- Modify the start.sh for your specific setup.
- Once setup is done then run egem start.sh

```
./prep.sh
./install.sh
./start.sh
```

# Add go to user profile
```
nano ~/.profile
```
- Enter the following at bottom of file.
```
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin
```

# Refresh Profile
```
source ~/.profile
```
# View Node
- Since the node is running in a screen you need to do the following command to view it.
- screen -r egemnode
- To leave the screen to have to do CTRL + A,D
