#!/bin/bash
#
# Basic EGEM Node install script (without extra features)
#

pre_checks(){
    if [ $EUID -ne 0 ]; then
    	echo
        echo ${RED}"Error: This script must be run as root. Terminating..."${NORMAL}
        echo
        exit 1
    fi

    case `uname -m` in
    "armv6l"|"armv7l")
        cpu_arch="armv6l"
    ;;
    "aarch64")
        cpu_arch="arm64"
    ;;
    "x86"|"i386"|"i686"|"32bit"|"32 bit")
        cpu_arch="386"
    ;;
    "x86_64"|"64bit"|"64 bit")
        cpu_arch="amd64"
    ;;
    *)
        error "detect CPU architecture (current one is not supported)"
    ;;
    esac

    total_ram=`free -m | grep -i "mem:" | awk '{print $2}'`

    if [ "${total_ram}" -lt "${min_ram}" ]; then
        echo ${RED}"Error: There is not enough ram to run a Quarry Node."
        echo ${YELLOW}"Minimum reqiured RAM: ${CYAN}${min_ram}MB"${NORMAL}
        echo ${YELLOW}"Current system RAM: ${RED}${total_ram}MB"${NORMAL}
        echo
        exit 1
    fi

    if [ "${total_ram}" -gt 1500 ]; then
        cache_size="1024"
    else
        cache_size=$((total_ram - 250))
	fi
}

show_warning(){
    clear
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    echo ${MAGENTA}"You are about to install EGEM Quarry Node from scratch."${NORMAL}
    echo
    echo ${YELLOW}"This will delete/overwrite previous Quarry Node files/folders."${NORMAL}
    echo
    echo ${MAGENTA}"Press ${GREEN}Enter${MAGENTA} to continue or press ${RED}Ctrl+C${MAGENTA} to terminate the script."${NORMAL}
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    read key
    clear
}

set_variables(){

    # --------------------------------------------------------------------------

#    export golang_version="1.13.8"
    export golang_dir="$HOME/.go"
    export GOPATH="${golang_dir}"

    # --------------------------------------------------------------------------

#    export min_ram="450"
#    export ideal_ram="1900"
    export ipcheck_url="ipinfo.io/ip"
    export swapfile="/swapfile"

    export package_manager="apt-get"
    export package_manager_parameters='--quiet -y'
    export DEBIAN_FRONTEND=noninteractive

    # --------------------------------------------------------------------------

    export static_nodes="static-nodes.json"
    export trusted_nodes="trusted-nodes.json"
    export dir_go_egem="${HOME}/go-egem"
    export dir_live_net="${HOME}/live-net"

    # --------------------------------------------------------------------------

    export egem_git="https://gitlab.com/ethergem/go-egem.git"
    export goegem_branch="master"
    export static_nodes_link="https://git.egem.io/team/egem-bootnodes/raw/master/${static_nodes}"
    export trusted_nodes_link="https://git.egem.io/team/egem-bootnodes/raw/master/${trusted_nodes}"

    # --------------------------------------------------------------------------

}

pkg(){
    command="${1}"
    ${package_manager} ${package_manager_parameters} ${command} &>/dev/null || ${package_manager} ${package_manager_parameters} ${command} &>/dev/null || error "${package_manager} ${command}"
}

create_swapfile(){
	header "Calculating needed RAM/Swap size..."

    # detect swap size
    current_swap=`swapon -se | grep -vi 'size' | awk '{s+=$3}END{print s}'`

    if [ -z "${current_swap}" ]; then
        current_swap="0"
    fi

    if [ "${current_swap}" -gt "0" ]; then
        current_swap=$(( ${current_swap} / 1024 ))
    fi

    # display RAM/swap info
    subheader "total RAM size  : ${YELLOW}${total_ram} MB"
    subheader "total SWAP size : ${YELLOW}${current_swap} MB"

    # calculate necessary swap size
	missing_swap="$((${ideal_ram}-(${total_ram}+${current_swap})))"

	if [ "${missing_swap}" -lt 100 ]; then
		missing_swap="0"
	fi

	if [ "${missing_swap}" -gt 0 ]; then
		subheader "creating a ${YELLOW}${missing_swap} MB${MAGENTA} swap file..."

		swapoff ${swapfile} &>/dev/null
		rm -rf ${swapfile} &>/dev/null

		dd if=/dev/zero of=${swapfile} bs=1MiB count=${missing_swap} &>/dev/null || error "create swap file (dd command)"

		chmod 600 ${swapfile} &>/dev/null
		mkswap ${swapfile} &>/dev/null || error "create swap file (mkswap)"

        subheader "activating new swap file..."

		# swapon ${swapfile} &>/dev/null || error "activate swap file (swapon)"
        swapon ${swapfile} &>/dev/null

        # if script couldn't activate swap file, check "total ram + total swap" size
        # continue if it is greater or equal than 900 MB, halt if it is less than 900 MB
        if [ $? != "0" ]; then
            if [ "$((${total_ram}+${current_swap}))" -lt 900 ]; then
                error "activate swap file (swapon)"
            fi
        fi

		# sed -i -e "s:.*${swapfile}.*::g" -e ":^$:d" /etc/fstab
		echo "$(grep -v ${swapfile} /etc/fstab)" > /etc/fstab
		echo "${swapfile} none swap sw 0 0" >> /etc/fstab

        swapon -a &>/dev/null
	else
		subheader "skipping swap file creation (you have enough RAM/Swap)..."

		return 0
	fi
}

install_necessary_packages(){
    header "Updating package database (might take a while)..."

        dpkg --clear-avail &>/dev/null

        pkg "update"

        yes | dpkg --configure -a &>/dev/null
        apt-get --quiet -y -f install &>/dev/null

        pkg "update"
        pkg "upgrade"

#    header "Removing golang (to install a custom version)..."

#        pkg "remove golang"
#        pkg "autoremove"

    header "Adding necessary repositories..."

        pkg "install software-properties-common"

        repository_list="main universe restricted multiverse"

        for r in ${repository_list}
        do
            add-apt-repository -y ${r} &>/dev/null
        done

    header "Installing necessary packages..."

        # workaround for a fail2ban error
        if [ ! -f /var/log/auth.log ]; then
            touch /var/log/auth.log &>/dev/null
            chmod 640 /var/log/auth.log &>/dev/null
        fi

        package_list="build-essential fail2ban git nano ntpdate openssh-server ufw wget"

        for pkg2install in ${package_list}
        do
            pkg "install ${pkg2install}"
        done

    header "Synchronizing time..."

	   ntpdate -s time.nist.gov
}

configure_firewall(){
	header "Configuring firewall..."

    if hash iptables-save 2>/dev/null; then
        if hash uniq 2>/dev/null; then
            { iptables-save | uniq | iptables-restore; } &>/dev/null
        else
            { iptables-save | grep -v 'dport 8895' | grep -v 'dport 8897' | grep -v 'dport 30666' | iptables-restore; } &>/dev/null
        fi
    else
        if hash sed 2>/dev/null; then
            { iptables -S | sed "/dport 8895/s/-A/iptables -D/e"; } &>/dev/null
            { iptables -S | sed "/dport 8897/s/-A/iptables -D/e"; } &>/dev/null
            { iptables -S | sed "/dport 30666/s/-A/iptables -D/e"; } &>/dev/null
        fi
    fi

    subheader "starting firewall..."

    systemctl daemon-reload &>/dev/null
    systemctl enable ufw &>/dev/null
    systemctl start ufw &>/dev/null

    subheader "setting ports/permissions..."

    # delete rules
    ufw delete allow 8895 &>/dev/null
    ufw delete allow 8897 &>/dev/null
    ufw delete allow 30666 &>/dev/null

    # add rules
    ufw default allow outgoing &>/dev/null
    ufw default deny incoming &>/dev/null
    ufw allow ssh &>/dev/null
    ufw limit ssh &>/dev/null
    ufw insert 1 allow 8895 &>/dev/null
    ufw insert 1 allow 8897 &>/dev/null
    ufw insert 1 allow 30666 &>/dev/null
    ufw logging on &>/dev/null
    ufw --force enable &>/dev/null

    if hash iptables 2>/dev/null; then
        # add rules again with iptables (a workaround for firewall conflicts)
        iptables -I INPUT 1 -p tcp --dport 30666 -j ACCEPT &>/dev/null
        iptables -I INPUT 1 -p udp --dport 30666 -j ACCEPT &>/dev/null

        iptables -I OUTPUT 1 -p tcp --dport 30666 -j ACCEPT &>/dev/null
        iptables -I OUTPUT 1 -p udp --dport 30666 -j ACCEPT &>/dev/null

        iptables -I INPUT 1 -p tcp --dport 8897 -j ACCEPT &>/dev/null
        iptables -I INPUT 1 -p udp --dport 8897 -j ACCEPT &>/dev/null

        iptables -I OUTPUT 1 -p tcp --dport 8897 -j ACCEPT &>/dev/null
        iptables -I OUTPUT 1 -p udp --dport 8897 -j ACCEPT &>/dev/null

        iptables -I INPUT 1 -p tcp --dport 8895 -j ACCEPT &>/dev/null
        iptables -I INPUT 1 -p udp --dport 8895 -j ACCEPT &>/dev/null

        iptables -I OUTPUT 1 -p tcp --dport 8895 -j ACCEPT &>/dev/null
        iptables -I OUTPUT 1 -p udp --dport 8895 -j ACCEPT &>/dev/null
    fi

    if hash iptables-save 2>/dev/null; then
        mkdir -p /etc/iptables/ &>/dev/null

        # save iptables firewall rules
        iptables-save 2>/dev/null >/etc/iptables/rules.v4
        ip6tables-save 2>/dev/null >/etc/iptables/rules.v6
    fi

    if hash netfilter-persistent 2>/dev/null; then
        netfilter-persistent start &>/dev/null
    fi
}

download_livenetwork_data(){
    header "Downloading necessary network files..."

    mkdir -p ${dir_live_net}/egem &>/dev/null
    cd ${dir_live_net}/egem

	subheader "trusted nodes list"

    rm -rf ${trusted_nodes}
	wget --no-check-certificate ${trusted_nodes_link} &>/dev/null || wget --no-check-certificate ${trusted_nodes_link} &>/dev/null || error "download trusted nodes list"

	subheader "static nodes list"

    rm -rf ${static_nodes}
	wget --no-check-certificate ${static_nodes_link} &>/dev/null || wget --no-check-certificate ${static_nodes_link} &>/dev/null || error "download static nodes list"

    cd ${HOME}
}

install_golang(){
    header "Installing Go-lang 1.18.8"
#
    # delete manually installed golang directory (leftover from previous setup)
    rm -rf ${golang_dir} &>/dev/null

    operating_system_type=`uname -s | tr -s '[:upper:]' '[:lower:]'`

    cd ${HOME}

	subheader "Downloading Golang"

	wget https://dl.google.com/go/go1.18.8.linux-amd64.tar.gz &>/dev/null || error "Downloading golang"

	subheader "Extracting archive"

	tar -xvf go1.18.8.linux-amd64.tar.gz &>/dev/null || error "Extracting archive"

	mv go ${GOPATH}
	

    { echo ${PATH} | grep ${GOPATH}/bin; } &>/dev/null || export PATH="${PATH}:${GOPATH}/bin"
    echo "export PATH="${PATH}:${GOPATH}/bin"" >> ~/.profile

	source ~/.profile

	#required for golang versions 1.16 and up for backwards compatibility
#	go env -w GO111MODULE=auto || error "go not found"

	go version

}

install_go_egem(){
	header "Installing Go-EGEM..."

	subheader "Download git"

	git clone ${egem_git} -b ${goegem_branch} &>/dev/null

    chmod -R 777 ${dir_go_egem} &>/dev/null
    cd ${dir_go_egem}

	#make sure go is installed properly
	go env -w GO111MODULE=auto || error "go not found"

	subheader "Building Egem"

	make all &>/dev/null || error "install Go-EGEM -> make egem"

	chmod +x stats &>/dev/null
	sudo \mv stats /usr/bin/ &>/dev/null

	chmod +x egem &>/dev/null
	sudo \mv egem /usr/bin/ &>/dev/null

    cd ${HOME}
}

start_node_service(){

	header "Starting node service"

cat &>/dev/null > /tmp/egemnode.service << EOL
[Unit]
Description=EGEM Node
After=network.target

[Service]

User=$_user
Group=$_user

Type=simple
Restart=always

ExecStart=$HOME/go-egem/build/bin/egem --syncmode=fast --cache=512 --rpc --rpcapi=web3,eth,net --rpccorsdomain=* --rpcaddr=0.0.0.0 --rpcvhosts=* --maxpeers=100

[Install]
WantedBy=default.target
EOL
}

start_egemstats_service(){

	header "Starting stats service"

cat &>/dev/null > /tmp/egemstats.service << EOL
[Unit]
Description=EGEM stats
After=network.target egemnode.service

[Service]

User=$_user
Group=$_user

Type=simple
Restart=always

ExecStart=$HOME/go-egem/build/bin/stats

[Install]
WantedBy=default.target
EOL
}

final_message(){
#    clear

	sudo \mv /tmp/egemnode.service /etc/systemd/system &>/dev/null
	sudo systemctl enable egemnode && sudo systemctl stop egemnode && sudo systemctl start egemnode &>/dev/null || error "Starting egemnode service"
	#systemctl status egemnode --no-pager --full

	sudo \mv /tmp/egemstats.service /etc/systemd/system &>/dev/null
	sudo systemctl enable egemstats && sudo systemctl stop egemstats && sudo systemctl start egemstats &>/dev/null || error "Starting egemstats service"
	#systemctl status egemstats --no-pager --full

    echo
    echo ${RED}"-------------------------------------------------------------------"${NORMAL}
	echo ${GREEN}"EGEM Node setup has finished succesfully!"${NORMAL}
	echo
	echo ${MAGENTA}"Public IP: ${YELLOW}`wget -q --no-check-certificate -O - ${ipcheck_url}`"${NORMAL}
    echo
	echo ${RED}"-------------------------------------------------------------------"${NORMAL}
	echo
	echo ${WHITE}"Check node with:"${NORMAL}
	echo
	echo "sudo journalctl -f -u egemnode"
	echo
}

header(){
	text="${1}"

    sleep 0.5s
    echo ${CYAN}"-------------------------------------------------------------------"
	echo "${WHITE}[${RED}*${WHITE}] ${GREEN}${text}${NORMAL}"

    sleep 0.5s
}

subheader(){
    text="${1}"

    echo "    ${YELLOW}> ${MAGENTA}${text}${NORMAL}"
    sleep 0.5s
}

error(){
	failed_step="${1}"

	echo ${RED}"Error! Install failed at this step ->  ${YELLOW}${failed_step}"${NORMAL}
    echo ${RED}"Please re-run the script. Contact developers if it happens again."${NORMAL}

    exit 1
}

# -----------------------------------------------------------------
# color codes for echo commands
NORMAL=$'\e[0m'
RED=$'\e[31;01m'
GREEN=$'\e[32;01m'
YELLOW=$'\e[33;01m'
BLUE=$'\e[34;01m'
MAGENTA=$'\e[35;01m'
CYAN=$'\e[36;01m'
WHITE=$'\e[97;01m'
# -----------------------------------------------------------------

cd ${HOME}

clear

pre_checks

shopt -s extglob
show_warning
set_variables
create_swapfile
install_necessary_packages
configure_firewall
download_livenetwork_data
install_golang
install_go_egem
start_node_service
start_egemstats_service
final_message
